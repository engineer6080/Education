################################################################################
# Automatically-generated file. Do not edit!
################################################################################

SHELL = cmd.exe

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../ArduCAM.cpp \
../main.cpp \
../pin_mux_config.cpp 

CMD_SRCS += \
../cc3200v1p32.cmd 

OBJS += \
./ArduCAM.obj \
./main.obj \
./pin_mux_config.obj 

CPP_DEPS += \
./ArduCAM.d \
./main.d \
./pin_mux_config.d 

OBJS__QUOTED += \
"ArduCAM.obj" \
"main.obj" \
"pin_mux_config.obj" 

CPP_DEPS__QUOTED += \
"ArduCAM.d" \
"main.d" \
"pin_mux_config.d" 

CPP_SRCS__QUOTED += \
"../ArduCAM.cpp" \
"../main.cpp" \
"../pin_mux_config.cpp" 


