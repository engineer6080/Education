from train import Train
from agent import DDPG
from jetcam.csi_camera import CSICamera
from Processimg import processimage
import numpy as np
import cv2
import time
import serial

#camera = CSICamera(width=int(1280/2), height=int(720/2)) #224
WIDTH = 244
HEIGHT = 244
camera = CSICamera(width=WIDTH, height=HEIGHT) #224
camera.running = True

training_module = Train(camera)

agent = DDPG((244, 244, 1), action_size=2)
count = 0

agent.load_model()

while True:
    with serial.Serial('/dev/ttyUSB0', 115200, timeout=10) as ser:
        image = training_module.getImage()
        count+=1
        #vertical_img = cv2.flip(image, -1)
        #out_img = cv2.cvtColor(vertical_img, cv2.COLOR_BGR2RGB)
        out_img = processimage(image, WIDTH, HEIGHT)
        out_img = np.expand_dims(out_img, axis=3)
        out_img = np.expand_dims(out_img, axis=0)

        test = agent.predict(out_img)
        rl_out = (test*[180,180]).astype('uint8')

        output = "{:05d}-{:05d}\n".format(rl_out[0][0], rl_out[0][1])
        ser.write(bytes(output,'utf-8'))
        if(count%100==0):
            print("Running")
        time.sleep(.050) #50 ms delay
