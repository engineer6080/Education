#!/bin/bash

while true; do
	OUTPUT="$(cat cmdfile)"
	if [ "$OUTPUT" == '1' ]; then
		echo "Restarting Tensorboard"
		killall tensorboard
		rm -rf logdir/*
		rm -rf scores/*
		echo "0" > cmdfile
		tensorboard --logdir=loss:logdir,score:scores&
	fi
done
