import numpy as np
from physics_sim import PhysicsSim
import math

# Go to location and Hover

class HoverTask():
    """Task (environment) that defines the goal and provides feedback to the agent."""
    def __init__(self, init_pose=None, init_velocities=None, 
        init_angle_velocities=None, runtime=5., target_pos=None):
        """Initialize a Task object.
        Params
        ======
            init_pose: initial position of the quadcopter in (x,y,z) dimensions and the Euler angles
            init_velocities: initial velocity of the quadcopter in (x,y,z) dimensions
            init_angle_velocities: initial radians/second for each of the three Euler angles
            runtime: time limit for each episode
            target_pos: target/goal (x,y,z) position for the agent
        """
        # Simulation
        self.sim = PhysicsSim(init_pose, init_velocities, init_angle_velocities, runtime) 
        print(init_pose)

        self.action_repeat = 1
        
        #ME
        self.reward_stat = []
        self.reward_matrix = []        
        self.mean_reward_matrix = [0,0,0]
        self.tolerance = [0,0,0]
        self.r_weight = [0,0,0]
        
        self.action_low = 0
        self.action_high = 400
        self.action_size = 4

        # Goal
        self.target_pos = target_pos if target_pos is not None else np.array([0., 0., 10.]) 
        self.state_size = self.action_repeat * (self.get_metric()).shape[0] #6
        
        self.init_pose = init_pose
        
        
    def sigmoid(self,x, fact):
          return 1 / (1 + math.exp(-fact*x))
        
    def set_target(self, r_weight, tolerance, target):
        self.r_weight = r_weight
        self.tolerance = tolerance
        self.target_pos = target
        

    def get_reward(self):
        """Uses current pose of sim to return reward."""
        
        # 300 m / 300 m / 300 m
        
        #Input: pose (6) for NN
        reward = 0
        euler_reward = 0
        proximity_reward = 0
        
        position = self.sim.pose[:3]
        euler_angles = self.sim.pose[3:]

        #element wise square
        f = np.subtract(position,self.target_pos[:3])
        
        x2 = -f**2
        pos = []
        
        for i, n in enumerate(self.target_pos):
            pos.append(max(1, int(abs(n))))
        
        res = np.divide(x2,pos)
        res = np.add(res, 3)
        parabolic_reward = res.sum()
        
        #degrees_angular = np.multiply(self.sim.angular_v, (180/3.14159))
        
        '''
        print("POS: ", pos,"\r")
        print("x2: ", self.target_pos[:3], x2,"\r")
        print("reward: ", reward,"\r")
        print("Angular: ", self.sim.angular_v,"\r")
        print("-----", "\r")
        '''
        
        #For hovering close
        squared_dist = np.sum((position-self.target_pos)**2, axis=0)
        dist = np.sqrt(squared_dist)
        
        #Const time reward
        #reward += 0.5
    
        
        if(dist <= 3):
            euler_reward = -((euler_angles).sum()) + 2
            proximity_reward = (5-dist)*10
            
        #Bounds Penalty
        '''
        for ii in range(3):
            if position[ii] <= self.lower_bounds[ii]:
                penalty += -1
            elif position[ii] > self.upper_bounds[ii]:
                penalty += -1
        '''
        
        reward = [parabolic_reward,proximity_reward,euler_reward]
        
        return reward
    
    def pretty(self, obj, labels=None):
        if(labels != None):
            for i, l in zip(obj,labels):
                print(l + " {:4.3f} ".format(i), end=" ")
        else:
            for i, n in enumerate(obj):
                print(str(i) + ": {:4.3f} ".format(n), end=" ")
            
        print("\n")
    
    # Changing state space (size still 6)
    def get_metric(self):
        # We want euler angles, phi, theta, psi and xyz velocity

        return self.sim.pose

    def step(self, rotor_speeds):
        """Uses action to obtain next state, reward, done."""
        
        reward = 0
        pose_all = []
        
        for _ in range(self.action_repeat):
            done = self.sim.next_timestep(rotor_speeds) # update the sim pose and velocities
            reward += np.array((self.get_reward())).sum()  # numpy bug ? ?  ???? sum = massive negative number
            pose_all.append(self.get_metric()) #self.sim.pose
            
        self.reward_stat.append(self.get_reward())            
        next_state = np.concatenate(pose_all) 

        if(done):
            matrix = np.asmatrix(np.array(self.reward_stat)) 
            self.reward_matrix = np.array([matrix[:,0],matrix[:,1],matrix[:,2]])
            self.mean_reward_matrix = np.array([matrix[:,0].mean(),matrix[:,1].mean(),matrix[:,2].mean()])
            
        
        return next_state, reward, done

    def reset(self):
        """Reset the sim to start a new episode."""
        self.sim.reset()
        state = np.concatenate([self.get_metric()] * self.action_repeat)        
        #clear stat
        self.reward_stat = []
        return state