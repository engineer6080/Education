import numpy as np
from physics_sim import PhysicsSim
from quadsim import QuadSim
import controller
import math
import random as rn

SEED = 123456
np.random.seed(SEED)
rn.seed(SEED)



class M_Task():
    """Task (environment) that defines the goal and provides feedback to the agent."""
    def __init__(self, init_pose=None, init_velocities=None, 
        init_angle_velocities=None, runtime=5., target_pos=None, factor=None):
        """Initialize a Task object.
        Params
        ======
            init_pose: initial position of the quadcopter in (x,y,z) dimensions and the Euler angles
            init_velocities: initial velocity of the quadcopter in (x,y,z) dimensions
            init_angle_velocities: initial radians/second for each of the three Euler angles
            runtime: time limit for each episode
            target_pos: target/goal (x,y,z) position for the agent
        """
        
        # Simulation
        #self.sim = PhysicsSim(init_pose, init_velocities, init_angle_velocities, runtime) 
        
        # Define the quadcopters
        QUADCOPTER={'q1':{'position':init_pose[:3],'orientation':[0,0,0],'L':0.3,'r':0.1,'prop_size':[10,4.5],'weight':1.2}}
        self.sim = QuadSim(QUADCOPTER)
        
        print(init_pose)

        self.action_repeat = 1
        
        # Stat reporting
        self.reward_stat = []
        self.reward_matrix = []        
        self.mean_reward_matrix = [0,0,0]
        
        # Default Dist params
        self.tolerance = [0,0,0]
        self.r_weight = [0,0,0]
        
        # Action space
        self.action_low = 4000
        self.action_high = 9000 #5500 #9000
        self.action_size = 4
        self.base_thrust = 400
        
        # For getting rotor speeds
        self.speeds = [0,0,0,0]
        self.hover_speeds = [0,0,0,0]
        
        # Store Rotor behaviors
        self.rotor_arr = []
        
        # Variable factor for debug
        self.factor = factor
        self.randomize = False
        
        # Goal
        self.target_pos = target_pos if target_pos is not None else np.array([0., 0., 10.]) 
        self.state_size = self.action_repeat * (self.get_metric()).shape[0]#6
        
        self.init_pose = init_pose
        self.runtime = runtime
        self.reached = False
        
        # Controller parameters
        CONTROLLER_PARAMETERS = {'Motor_limits':[4000,9000], #9000
                        'Tilt_limits':[-10,10],
                        'Yaw_Control_Limits':[-900,900],
                        'Z_XY_offset':500,
                        'Linear_PID':{'P':[300,300,7000],'I':[0.04,0.04,4.5],'D':[450,450,5000]},
                        'Linear_To_Angular_Scaler':[1,1,0],
                        'Yaw_Rate_Scaler':0.18,
                        'Angular_PID':{'P':[22000,22000,1500],'I':[0,0,1.2],'D':[12000,12000,0]},
                        }
        self.ctr = controller.Controller_PID_Point2Point(self.sim.get_state,self.sim.get_time,self.sim.set_motor_speeds,
                                                      params=CONTROLLER_PARAMETERS,quad_identifier='q1')

        
    # Error term from paper
    # http://www.mit.edu/~shayegan/files/omidshafiei_quadrotor_control13.pdf
    def exponentialError(self, pos, target, v_, range_):
        V_RANGE = 15.
        first_term = math.exp(-(abs(pos - target)/(0.3 * range_)))
        second_term = math.exp(-(abs(v_)/(0.1 * V_RANGE)))
        err = first_term * second_term
        return err
        
    # General purpose
    def exponentialError2(self, pos, target, range_, maxreward):
        err = 0.
        for i,n in enumerate(pos): # divide by zero
            first_term = maxreward * math.exp(-(abs(pos[i] - target[i])/(0.3 * range_)))
            err += first_term
        return err
    
    # Sigmoid function with optional param
    def sigmoid(self,x, fact=1):
          return 1 / (1 + math.exp(-fact*x))
        
    # Set Params
    def set_params(self, r_weight, tolerance, init_pose, target):
        self.r_weight = r_weight
        self.tolerance = tolerance
        self.target_pos = target
        self.init_pose = init_pose
        self.hover()
        
    def hover(self):
        self.ctr.update_target(self.target_pos)
        #self.ctr.update_target(self.sim.get_position('q1'))
        self.ctr.update_yaw_target(0.)
        
    # Error function attempt 1
    def parabolicError(self, position, target, maxreward=[3,3,3], tolerance=[1,1,1]):
        zx = np.multiply(position,tolerance)
        zxt = -(np.subtract(zx,target) ** 2)
        zxtr = np.add(zxt, maxreward)

        # Proportionally penalize for deviating from target outside parabolic bound
        for i, x in enumerate(zxtr):
            zxtr[i] = max(-maxreward[i], x)
            
        return zxtr        
    
    # Simple reward function
    def sparse_reward(self, limit):
        x1 = 0; y1 = 0; z1 = self.target_pos[2]
        pos = self.sim.pose[:3]
        return np.sqrt((x1-pos[0])**2 + (y1-pos[1])**2 + (z1-pos[2])**2) < limit
    
    # Error function attempt 2
    def reciprocalError(self, point, target, maxreward, tolerance=[1,1,1]):
        # 1/x
        err = np.zeros(point.shape)
        f = np.absolute(np.subtract(point,target))  # 1/(x-target)
        for i,n in enumerate(f): # divide by zero
            if n == 0:
                err[i] = maxreward[i]
            else:
                err[i] = ((1.0*tolerance[i]) / (n))  -maxreward[i]
                err[i] = min(err[i], maxreward[i]) # max reward
                #err[i] = max(err[i], 0) # min reward
        return err

    
    # Error function attempt 3
    def absError(self, point, target, maxreward, tolerance=[1,1,1]):
        z = np.absolute(np.subtract(point,target))
        zx = -np.multiply(z, tolerance)
        zxr = np.add(zx, maxreward)
        for i,n in enumerate(zxr):
            zxr[i] = max(-maxreward[i], n)
        return zxr
    
    def euler_to_quaternion(self, roll, pitch, yaw):
        qx = np.sin(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) - np.cos(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
        qy = np.cos(roll/2) * np.sin(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.cos(pitch/2) * np.sin(yaw/2)
        qz = np.cos(roll/2) * np.cos(pitch/2) * np.sin(yaw/2) - np.sin(roll/2) * np.sin(pitch/2) * np.cos(yaw/2)
        qw = np.cos(roll/2) * np.cos(pitch/2) * np.cos(yaw/2) + np.sin(roll/2) * np.sin(pitch/2) * np.sin(yaw/2)
        return [qx, qy, qz, qw]
    
    # Calculates Rotation Matrix given euler angles.
    # https://www.learnopencv.com/rotation-matrix-to-euler-angles/
    def eulerAnglesToRotationMatrix(self, theta) :
        R_x = np.array([[1,         0,                  0                   ],
                        [0,         math.cos(theta[0]), -math.sin(theta[0]) ],
                        [0,         math.sin(theta[0]), math.cos(theta[0])  ]
                        ])

        R_y = np.array([[math.cos(theta[1]),    0,      math.sin(theta[1])  ],
                        [0,                     1,      0                   ],
                        [-math.sin(theta[1]),   0,      math.cos(theta[1])  ]
                        ])

        R_z = np.array([[math.cos(theta[2]),    -math.sin(theta[2]),    0],
                        [math.sin(theta[2]),    math.cos(theta[2]),     0],
                        [0,                     0,                      1]
                        ])

        R = np.dot(R_z, np.dot( R_y, R_x ))
        return R

    def get_reward(self):
        """Uses current pose of sim to return reward."""
        reward = 0                
        position = np.copy(self.sim.get_position('q1'))
        velocity = self.sim.get_linear_rate('q1') 
        euler_angles = self.sim.get_orientation('q1')
        
        # Main reward
        dist = np.linalg.norm(position-self.target_pos)
        #dist_reward = self.reciprocalError(np.array([dist]), [0.], self.r_weight, self.tolerance)
        #dist_reward = sum(dist_reward)/self.r_weight[0]
        
        # Upright
        e_weight = [100,100,100] #14
        e_tolerance = [1000,1000,1000] #7
        euler_reward = self.reciprocalError(euler_angles, [0.,0.,0.], e_weight, e_tolerance)

        # Calculate angular deviation
        ang_dev = sum(euler_reward)/sum(e_weight)
                
        # Penalizes Position and Velocity 
        x_reward = self.exponentialError(position[0], self.target_pos[0], velocity[0], 15.)
        y_reward = self.exponentialError(position[1], self.target_pos[1], velocity[1], 15.)
        z_reward = self.exponentialError(position[2], self.target_pos[2], velocity[2], 15.) 
        #dist_reward = (x_reward + y_reward + z_reward)/3.
        
        #deviation = np.linalg.norm(self.speeds-self.hover_speeds)
        #deviation_reward = self.exponentialError2(self.speeds, self.hover_speeds, 500., 5.)
        #deviation_reward = deviation_reward/20.
        
        dist_reward = self.exponentialError2(position, self.target_pos, 10., 2.)/6.
        
        # Reward for reaching target
        if(dist <= 1):
            #dist_reward += 1
            self.reached = True
        
        # Weights
        a_r = ang_dev * 0
        
        reward = [dist_reward, 0., 0.]

        return reward
    
    def pretty(self, obj, labels=None):
        if(labels != None):
            for i, l in zip(obj,labels):
                print(l + " {:4.3f} ".format(i), end=" ")
        else:
            for i, n in enumerate(obj):
                print(str(i) + ": {:4.3f} ".format(n), end=" ")
        print("\n")
        
    def normalize(self, x, low, high):
        normalized = np.multiply(np.divide(np.subtract(x,low),np.subtract(high,low)),[1,1,1])
        return normalized

    # Changing state space
    def get_metric(self):
        # Normalize: ~approx
        dist_err = np.subtract(self.sim.get_position('q1'),self.target_pos)
        orientation = self.sim.get_orientation('q1')
        angular = self.sim.get_angular_rate('q1')
        linear = self.sim.get_linear_rate('q1')

        vec = [dist_err, orientation, angular, linear]
        out = []
                
        for i, active in enumerate(self.factor):
            if(active == 1):
                out = np.concatenate((out, vec[i]))

        return np.array(out)
        
    def step(self, rotor_speeds):
        """Uses action to obtain next state, reward, done."""
        reward = 0.
        pose_all = []
        
        rewards = [0.,0.,0.]
        done = False
        self.speeds = np.copy(rotor_speeds)
        
        for _ in range(self.action_repeat):            
            self.sim.set_motor_speeds('q1', rotor_speeds) # update the sim pose and velocities
            bounds = self.sim.update(1/50.)
            rewards = self.get_reward()
            
            if (self.sim.time > self.runtime) or bounds:
                #rewards = [-1.,0.,0.]
                done = True
            elif(self.reached):
                print("Success: End: ", self.sim.pose[:3])
                done = True

            if bounds:
                print("Out of bounds: ", self.sim.pose[:3])
                
            reward += sum(rewards)
            self.reward_stat.append(rewards)
                
            # For plotting
            self.rotor_arr.append(rotor_speeds)
            
            pose_all.append(self.get_metric())
            
        next_state = np.concatenate(pose_all) 
       
        # Some more plotting and stats stuff
        if(done):
            # Rewards
            matrix = np.asmatrix(np.array(self.reward_stat)) 
            self.reward_matrix = np.array([matrix[:,0],matrix[:,1],matrix[:,2]])
            self.mean_reward_matrix = np.array([matrix[:,0].mean(),matrix[:,1].mean(),matrix[:,2].mean()])
            # Rotor Speeds
            rotor_matrix = np.asmatrix(np.array(self.rotor_arr)) 
            self.rotor_arr = np.array([rotor_matrix[:,0],rotor_matrix[:,1],rotor_matrix[:,2],rotor_matrix[:,3]])
            
        return next_state, reward, done

    def reset(self):
        """Reset the sim to start a new episode."""
        self.sim.reset()
        self.sim.position = np.zeros(12)
        
        self.reached = False
        # -10 to 10 (x,y)
        # 0 to 10 (z)
        if(self.randomize): # New start location
            rx = rn.randint(-9,9)
            ry = rn.randint(-9,9)
            rz = rn.randint(1,9)
            print("New start: ", [rx,ry,rz])
            self.sim.set_position('q1', [rx,ry,rz])
        else:
            self.sim.set_position('q1', self.init_pose[:3])
            
        self.sim.set_orientation('q1', self.init_pose[3:])
        self.sim.time = 0.
        state = np.concatenate([self.get_metric()] * self.action_repeat)        
        # Refresh inner episodic stats
        self.reward_stat = []
        self.rotor_arr = []
        return state