from keras import layers, models, optimizers, regularizers
from keras import backend as K
from keras.layers import Flatten, Concatenate, LeakyReLU
from keras.utils.generic_utils import get_custom_objects
from keras.initializers import RandomUniform, Zeros
from keras_radam import RAdam
import numpy as np

SEED = 123456
np.random.seed(SEED)

def mish(x):
    return x*K.tanh(K.softplus(x))

get_custom_objects().update({'Mish': layers.Activation(mish)})

class Critic:
    """Critic (Value) Model."""

    def __init__(self, state_size, action_size, lr, network):
        """Initialize parameters and build model.

        Params
        ======
            state_size (int): Dimension of each state
            action_size (int): Dimension of each action
        """
        self.state_size = state_size
        self.action_size = action_size
        self.lr = lr
        self.network = network
        self.build_model()

    def build_model(self):
        """Build a critic (value) network that maps (state, action) pairs -> Q-values."""
        
        # Define input layers
        states = layers.Input(shape=(self.state_size,), name='states')
        actions = layers.Input(shape=(self.action_size,), name='actions')
        
        #actions_norm = layers.BatchNormalization()(actions) , kernel_regularizer=layers.regularizers.l2(regularization)
        #states_actions = Concatenate()([states, actions])
        #net_states = layers.BatchNormalization()(states_actions)
        
        alp = 0.1
        
        n1 = 3
        n2 = 5
        
        state1 = layers.Lambda(lambda x: x[:,0:3])(states)
        state2 = layers.Lambda(lambda x: x[:,3:6])(states)
        state3 = layers.Lambda(lambda x: x[:,6:9])(states)
        state4 = layers.Lambda(lambda x: x[:,9:12])(states)

        net1 = layers.Dense(units=n1)(state1)
        net1 = layers.LeakyReLU(alpha=alp)(net1)
        net1 = layers.Dense(units=1)(net1)
        
        net2 = layers.Dense(units=n1)(state2)
        net2 = layers.LeakyReLU(alpha=alp)(net2)
        net2 = layers.Dense(units=1)(net2)
        
        net3 = layers.Dense(units=n1)(state3)
        net3 = layers.LeakyReLU(alpha=alp)(net3)
        net3 = layers.Dense(units=1)(net3)
        
        net4 = layers.Dense(units=n1)(state4)
        net4 = layers.LeakyReLU(alpha=alp)(net4)
        net4 = layers.Dense(units=1)(net4)
        
        action = layers.Dense(units=4)(actions)
        action = layers.LeakyReLU(alpha=alp)(action)
        action = layers.Dense(units=1)(action)
        
        states_actions = Concatenate()([net1, net2, net3, net4, action])
        states_actions = layers.BatchNormalization()(states_actions)
        net = layers.Dense(units=n2)(states_actions)
        net = layers.LeakyReLU(alpha=alp)(net)
        
        #net = layers.Activation('Mish')(net) 

        # Combine state and action pathways
        # Add final output layer to produce action values (Q values)
        #Q_values = layers.Dense(units=1, name='q_values', kernel_initializer=RandomUniform())(net)Zeros()
        Q_values = layers.Dense(units=1, name='q_values', kernel_initializer=RandomUniform())(net)
        #Q_values = layers.LeakyReLU(alpha=alp)(Q_values)

        # Create Keras model
        self.model = models.Model(inputs=[states, actions], outputs=Q_values)

        # Define optimizer and compile model for training with built-in loss function
        #optimizer = optimizers.Adam(self.lr)
        optimizer = RAdam()
        self.model.compile(optimizer=optimizer, loss='mse')

        # Compute action gradients (derivative of Q values w.r.t. to actions)
        action_gradients = K.gradients(Q_values, actions)

        # Define an additional function to fetch action gradients (to be used by actor model)
        self.get_action_gradients = K.function(
            inputs=[*self.model.input, K.learning_phase()],
            outputs=action_gradients)