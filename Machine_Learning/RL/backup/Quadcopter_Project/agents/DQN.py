import random
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from keras import layers, models, optimizers, regularizers
# Deep Q-learning Agent

class DQNAgent:
    def __init__(self, task, train=True):
        
        self.task = task
        self.state_size = task.state_size
        self.action_size = task.action_size
        self.action_low = task.action_low
        self.action_high = task.action_high
        self.action_range = self.action_high - self.action_low
        
        self.memory = deque(maxlen=2000)
        self.batch_size = 64
        self.gamma = 0.95    # discount rate
        self.epsilon = 1.0  # exploration rate
        self.epsilon_min = 0.01
        self.epsilon_decay = 0.995
        self.learning_rate = 0.001
        self.model = self._build_model()
        self.train = train
        
       
        
        print("DQN init")
        
        
    def _build_model(self):
        # Neural Net for Deep-Q learning Model
        model = Sequential()
       # Define input layer (states)
        states = layers.Input(shape=(self.state_size,), name='states')
        
        # Add hidden layers
        net = layers.Dense(units=32, activation='relu', kernel_regularizer=layers.regularizers.l2(1e-6))(states)
        net = layers.Dense(units=64, activation='relu')(net)
        net = layers.Dense(units=32, activation='relu')(net)
        
        # Add final output layer with sigmoid activation
        raw_actions = layers.Dense(units=self.action_size, activation='sigmoid',
            name='raw_actions')(net)
        
        # Scale [0, 1] output for each action dimension to proper range
        actions = layers.Lambda(lambda x: (x * self.action_range) + self.action_low,
            name='actions')(raw_actions)
        
        # Create Keras model
        model = models.Model(inputs=states, outputs=actions)

        model.compile(loss='mse',
                      optimizer=Adam(lr=self.learning_rate))
        return model
    
    def reset_episode(self):
        state = self.task.reset()
        self.last_state = state
        return state
    
    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))
        
        
    def act(self, state):
        #if np.random.rand() <= self.epsilon:
        #    return random.randrange(self.action_size)
        state = np.reshape(state, [-1, self.state_size])
        action = self.model.predict(state)[0]
        return action  # returns action
    
    
    def step(self, action, reward, next_state, done):
        if self.train:
             # Save experience / reward
            self.remember(self.last_state, action, reward, next_state, done)

            # Learn, if enough samples are available in memory
            if len(self.memory) > self.batch_size:
                self.replay()

        # Roll over last state and action
        self.last_state = next_state

    
    def replay(self):
        minibatch = random.sample(self.memory, self.batch_size)
        for state, action, reward, next_state, done in minibatch:
            target = reward
            print("nd", next_state.shape)
            if not done:
              target = reward + self.gamma * \
                       self.model.predict(next_state)[0]
            print(state)
            target_f = self.model.predict(state)[0]
            target_f[0][action] = target
            self.model.fit(state, target_f, epochs=1, verbose=0)
        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay