import tensorflow as tf
import tensorflow.keras as keras
import numpy as np
import copy
from collections import namedtuple, deque
from Actor import Actor
from MyCritic import Critic
from Custom import Custom
from keras.callbacks import EarlyStopping, ModelCheckpoint

from Noise import OUNoise

SEED = 123456
import random as rn
from tensorflow import set_random_seed

np.random.seed(SEED)
set_random_seed(SEED)
rn.seed(SEED)


# Transform train_on_batch return value
# to dict expected by on_batch_end callback
def named_logs(model, logs):
    result = {}
    for l in zip(model.metrics_names, logs):
        result[l[0]] = l[1]
    return result

def tfSummary(tag, val):
    """ Scalar Value Tensorflow Summary
    """
    return tf.Summary(value=[tf.Summary.Value(tag=tag, simple_value=val)])

class ReplayBuffer:
    """Fixed-size buffer to store experience tuples."""

    def __init__(self, buffer_size, batch_size):
        """Initialize a ReplayBuffer object.
        Params
        ======
            buffer_size: maximum size of buffer
            batch_size: size of each training batch
        """
        self.memory = deque(maxlen=buffer_size)  # internal memory (deque)
        self.batch_size = batch_size
        self.experience = namedtuple("Experience", field_names=["state", "action", "reward", "next_state", "done"])
        
    def sortKey(self,e):
        return e.reward
    
    def add(self, state, action, reward, next_state, done):
        """Add a new experience to memory."""
        e = self.experience(state, action, reward, next_state, done)
        self.memory.append(e)

    def sample(self, batch_size=64):
        """Randomly sample a batch of experiences from memory."""
        return rn.sample(self.memory, k=self.batch_size)

    def __len__(self):
        """Return the current size of internal memory."""
        return len(self.memory)


class DDPG():
    """Reinforcement Learning agent that learns using DDPG."""
    def __init__(self, task, train=True):
        self.task = task
        self.state_size = task.state_size
        self.action_size = task.action_size
        self.action_low = task.action_low
        self.action_high = task.action_high
        
        self.actor_lr =  1e-6 #.0001 6
        self.critic_lr = 1e-6 #0.0000001 6
        regularize = 1e-9     #1e-9
        
        self.network = [128,256,128]
        
        self.train = train
        network = self.network
        actor_lr = self.actor_lr
        critic_lr = self.critic_lr 
        network = self.network
        
        if(self.train):
            # Actor (Policy) Model
            self.actor_local = Actor(self.state_size, self.action_size, self.action_low, self.action_high, actor_lr, network)
            self.actor_target = Actor(self.state_size, self.action_size, self.action_low, self.action_high, actor_lr, network)

            # Critic (Value) Model
            self.critic_local = Critic(self.state_size, self.action_size, critic_lr, network)
            self.critic_target = Critic(self.state_size, self.action_size, critic_lr, network)

            # Initialize target model parameters with local model parameters
            self.critic_target.model.set_weights(self.critic_local.model.get_weights())
            self.actor_target.model.set_weights(self.actor_local.model.get_weights())

            # Noise process
            self.exploration_mu = 0 # Mean
            self.exploration_theta = 0.15 #.15 How fast variable reverts to mean
            self.exploration_sigma = 0.01 #.2 Degree of volatility
            self.noise = OUNoise(self.action_size, self.exploration_mu, self.exploration_theta, self.exploration_sigma)

            # Replay memory
            self.buffer_size = 100000
            self.batch_size = 32
            self.memory = ReplayBuffer(self.buffer_size, self.batch_size)
            self.targets = ReplayBuffer(self.buffer_size, self.batch_size)

            # Algorithm parameters
            self.gamma = 0.99  # discount factor
            self.tau = 0.002   # for soft update of target parameters
            
            self.guide = False
        

            print("DDPG init", "Actor: ", actor_lr, "Critic: ", critic_lr, "Regularization: ", regularize)
            print("Tau: ", self.tau, "Sigma: ", self.exploration_sigma)
            #print(self.actor_local.model.summary())
            #print(self.critic_local.model.summary())

            # https://stackoverflow.com/questions/44861149/keras-use-tensorboard-with-train-on-batch?rq=1
            # Create the TensorBoard callback,
            # which we will drive manually
            self.tensorboard = keras.callbacks.TensorBoard(
              log_dir='logdir',
              histogram_freq=0,
              batch_size=self.batch_size,
              write_graph=True,
              write_grads=True
            )

            self.tensorboard.set_model(self.critic_local.model)
            self.summary_writer = tf.summary.FileWriter("scores")

            self.batch_id = 0
            self.freeze = False
        
    def reset_episode(self):
        if(self.train):
            self.noise.reset()
            self.noise_arr = []
            self.noise_matrix = [0.,0.,0.,0.]
        
        state = self.task.reset()
        self.last_state = state
        return state
    
    def save_initial_weights(self):
        self.actor_local.model.save_weights('actor_local.h5')
        self.actor_target.model.save_weights('actor_target.h5')
        self.critic_local.model.save_weights('critic_local.h5')
        self.critic_target.model.save_weights('critic_target.h5')
                                              
    def load_initial_weights(self):
        self.actor_local.model.load_weights('actor_local.h5')
        self.actor_target.model.load_weights('actor_target.h5')
        self.critic_local.model.load_weights('critic_local.h5')
        self.critic_target.model.load_weights('critic_target.h5')
        
    def save_model(self):
        # Save the weights
        self.actor_local.model.save_weights('model_weights.h5')
        # Save the model architecture
        #with open('model_architecture.json', 'w') as f:
        #    f.write(self.actor_local.model.to_json())

    def load_weights(self, option=None):
        if(option==None):
            self.trained = Actor(self.state_size, self.action_size, self.action_low, 
                                 self.action_high, self.actor_lr, self.network)
            self.trained.model.load_weights('model_weights.h5')
        else:
            self.trained = Actor(self.state_size, self.action_size, self.action_low, 
                     self.action_high, self.actor_lr, self.network)
            self.trained.model.load_weights('weights-best.hdf5')
            print(self.trained.model.summary())
        
        
    def predict(self, state):
        """Returns actions for given state(s) as per current policy."""
        state = np.reshape(state, [-1, self.state_size])
        action = self.trained.model.predict(state)[0]
        return action
        
    def step(self, action, reward, next_state, done):
        # Save experience / reward
        if(self.freeze == False):
            self.memory.add(self.last_state, action, reward, next_state, done)

        # Learn, if enough samples are available in memory
        if(len(self.memory) > self.batch_size):
        #if(len(self.memory) == self.buffer_size):
        #    print("Buffer Full")
            experiences = self.memory.sample()
            self.learn(experiences)
                
        # Roll over last state and action
        self.last_state = next_state

    def act(self, state):
        """Returns actions for given state(s) as per current policy."""
        state = np.reshape(state, [-1, self.state_size])
        if self.guide:
            action = self.task.ctr.update()
        else:
            action = self.actor_local.model.predict(state)[0]
        
        noise = self.noise.sample()

        return list(action + noise), noise  # add some noise for exploration
        
    def progress_test(self, state):
        action = self.actor_local.model.predict(state)[0]
        return list(action)
    
    
    def NN_learn(self, num_epochs):
        experiences = self.memory.memory
        print("BUFFER SIZE", len(self.memory))
        
        states = np.vstack([e.state for e in experiences if e is not None])
        actions = np.array([e.action for e in experiences if e is not None]).astype(np.float32).reshape(-1, self.action_size)

        cut = int(len(self.memory) * 0.3)
        testX = states[-cut:]
        testY = actions[-cut:]
        
        #earlystop = EarlyStopping(monitor='val_loss', patience=2)
        filepath="weights-improvement-{epoch:02d}-{val_loss:.2f}.hdf5"
        # Lambdas (weights only)
        checkpoint = ModelCheckpoint(filepath, monitor='val_loss', save_best_only=True, save_weights_only=True)
        
        # tensorboard = keras.callbacks.TensorBoard(log_dir='scores', histogram_freq=0,
        #                  write_graph=True, write_images=False)
        
        callbacks_list = [checkpoint]
        self.custom.model.fit(x=[states[:-cut]], y=actions[:-cut], epochs=num_epochs, callbacks=callbacks_list, validation_data=(testX, testY)) #batch_size=self.batch_size
        
        self.memory.memory.clear()
        

    def learn(self,experiences): #experiences 
        """Update policy and value parameters using given batch of experience tuples."""
        # Convert experience tuples to separate arrays for each element (states, actions, rewards, etc.)
        states = np.vstack([e.state for e in experiences if e is not None])
        actions = np.array([e.action for e in experiences if e is not None]).astype(np.float32).reshape(-1, self.action_size)
        rewards = np.array([e.reward for e in experiences if e is not None]).astype(np.float32).reshape(-1, 1)
        dones = np.array([e.done for e in experiences if e is not None]).astype(np.uint8).reshape(-1, 1)
        next_states = np.vstack([e.next_state for e in experiences if e is not None])
        
        
        print("States", states.shape)
        print("actions", actions.shape)
        print("rewards", rewards.shape)
        print("dones", dones.shape)
        print("Next states", next_states.shape)
        # keep training actor local and critic local
        # use values from target model to update and train local
        # don't train target models, we soft update target

        # Get predicted next-state actions and Q values from target models
        #     Q_targets_next = critic_target(next_state, actor_target(next_state))
        
        actions_next = self.actor_target.model.predict_on_batch(next_states) #target
                    
        #Actions predicted by target critic
        Q_targets_next = self.critic_target.model.predict_on_batch([next_states, actions_next]) #target

        # Compute Q targets for current states and train critic model (local)
        Q_targets = rewards + self.gamma * Q_targets_next * (1 - dones)
        
        critic_loss = self.critic_local.model.train_on_batch(x=[states, actions], y=Q_targets)
        
        # Train actor model (local)
        action_gradients = np.reshape(self.critic_local.get_action_gradients([states, actions, 0]), (-1, self.action_size))
        actor_loss = self.actor_local.train_fn([states, action_gradients, 1])  # custom training function
        
        self.tensorboard.on_epoch_end(self.batch_id, named_logs(self.critic_local.model, [critic_loss]))
        self.batch_id += 1
        
        # Soft-update target models
        self.soft_update(self.critic_local.model, self.critic_target.model)
        self.soft_update(self.actor_local.model, self.actor_target.model)   

    def soft_update(self, local_model, target_model):
        """Soft update model parameters."""
        local_weights = np.array(local_model.get_weights())
        target_weights = np.array(target_model.get_weights())

        assert len(local_weights) == len(target_weights), "Local and target model parameters must have the same size"

        new_weights = self.tau * local_weights + (1 - self.tau) * target_weights
        target_model.set_weights(new_weights)