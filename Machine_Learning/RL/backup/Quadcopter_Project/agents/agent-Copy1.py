import tensorflow as tf
import tensorflow.keras as keras
import numpy as np
import copy
from collections import namedtuple, deque
from memory_buffer import MemoryBuffer

from Actor import Actor
from MyCritic import Critic
from Noise import OUNoise

SEED = 123456
import random as rn
from tensorflow import set_random_seed

np.random.seed(SEED)
set_random_seed(SEED)
rn.seed(SEED)



# Transform train_on_batch return value
# to dict expected by on_batch_end callback
def named_logs(model, logs):
    result = {}
    for l in zip(model.metrics_names, logs):
        result[l[0]] = l[1]
    return result

def tfSummary(tag, val):
    """ Scalar Value Tensorflow Summary
    """
    return tf.Summary(value=[tf.Summary.Value(tag=tag, simple_value=val)])

class ReplayBuffer:
    
    """Fixed-size buffer to store experience tuples."""

    def __init__(self, buffer_size, batch_size):
        """Initialize a ReplayBuffer object.
        Params
        ======
            buffer_size: maximum size of buffer
            batch_size: size of each training batch
        """
        self.memory = deque(maxlen=buffer_size)  # internal memory (deque)
        self.batch_size = batch_size
        self.experience = namedtuple("Experience", field_names=["state", "action", "reward", "next_state", "done"])
        
    def sortKey(self,e):
        return e.reward
    
    def add(self, state, action, reward, next_state, done):
        """Add a new experience to memory."""
        e = self.experience(state, action, reward, next_state, done)
        self.memory.append(e)

    def sample(self, batch_size=64):
        """Randomly sample a batch of experiences from memory."""
        return rn.sample(self.memory, k=self.batch_size)
        #temp = list(self.memory)
        #temp.sort(key = self.sortKey, reverse = True)
        #return (temp[:self.batch_size])

    def __len__(self):
        """Return the current size of internal memory."""
        return len(self.memory)


class DDPG():
    """Reinforcement Learning agent that learns using DDPG."""
    def __init__(self, task, parameter=0, train=True):
        self.task = task
        self.state_size = task.state_size
        self.action_size = task.action_size
        self.action_low = task.action_low
        self.action_high = task.action_high
        
        self.actor_lr =  1e-6 #.0001 6
        self.critic_lr = 1e-6 #0.0000001 6
        regularize = 1e-9     #1e-9
        
        self.network = [128,256,128]
        
        network = self.network
        actor_lr = self.actor_lr
        critic_lr = self.critic_lr 
        network = self.network
        
        #np.random.seed(1337)
        
        # Actor (Policy) Model
        self.actor_local = Actor(self.state_size, self.action_size, self.action_low, self.action_high, actor_lr, network)
        self.actor_target = Actor(self.state_size, self.action_size, self.action_low, self.action_high, actor_lr, network)

        # Critic (Value) Model
        self.critic_local = Critic(self.state_size, self.action_size, critic_lr, network)
        self.critic_target = Critic(self.state_size, self.action_size, critic_lr, network)

        # Initialize target model parameters with local model parameters
        self.critic_target.model.set_weights(self.critic_local.model.get_weights())
        self.actor_target.model.set_weights(self.actor_local.model.get_weights())

        # Noise process
        self.exploration_mu = 0 # Mean
        self.exploration_theta = 0.15 #.15 How fast variable reverts to mean
        self.exploration_sigma = 0.01 #.2 Degree of volatility
        self.noise = OUNoise(self.action_size, self.exploration_mu, self.exploration_theta, self.exploration_sigma)

        # Replay memory
        self.buffer_size = 10000 
        self.batch_size = 32 #64
        self.memory = ReplayBuffer(self.buffer_size, self.batch_size)
        #self.buffer = MemoryBuffer(self.buffer_size, True)

        # Algorithm parameters
        self.gamma = 0.99  # discount factor
        self.tau = 0.002   # for soft update of target parameters
        
        self.noise_arr = []
        self.noise_matrix = [0.,0.,0.,0.]

        print("DDPG init", "Actor: ", actor_lr, "Critic: ", critic_lr, "Regularization: ", regularize)
        print("Tau: ", self.tau, "Sigma: ", self.exploration_sigma)
        print(self.actor_local.model.summary())
        print(self.critic_local.model.summary())
        
        # https://stackoverflow.com/questions/44861149/keras-use-tensorboard-with-train-on-batch?rq=1
        # Create the TensorBoard callback,
        # which we will drive manually
        self.tensorboard = keras.callbacks.TensorBoard(
          log_dir='logdir',
          histogram_freq=0,
          batch_size=self.batch_size,
          write_graph=True,
          write_grads=True
        )

        self.tensorboard.set_model(self.critic_local.model)
        self.summary_writer = tf.summary.FileWriter("scores")

        self.batch_id = 0
        
        self.train = train

    def reset_episode(self):
        self.noise.reset()
        state = self.task.reset()
        self.last_state = state
        self.noise_arr = []
        self.noise_matrix = [0.,0.,0.,0.]
        return state
    
    def save_initial_weights(self):
        self.actor_local.model.save_weights('actor_local.h5')
        self.actor_target.model.save_weights('actor_target.h5')
        self.critic_local.model.save_weights('critic_local.h5')
        self.critic_target.model.save_weights('critic_target.h5')
                                              
    def load_initial_weights(self):
        self.actor_local.model.load_weights('actor_local.h5')
        self.actor_target.model.load_weights('actor_target.h5')
        self.critic_local.model.load_weights('critic_local.h5')
        self.critic_target.model.load_weights('critic_target.h5')
        
    def save_model(self):
        # Save the weights
        self.actor_local.model.save_weights('model_weights.h5')
        # Save the model architecture
        #with open('model_architecture.json', 'w') as f:
        #    f.write(self.actor_local.model.to_json())

    def load_weights(self):
        self.actor_local.model.load_weights('model_weights.h5')
        
        
    def step(self, action, reward, next_state, done):
        
        if self.train:
            
            last_state1 = np.array([self.last_state]).reshape(-1, self.state_size)
            action1 = np.array([action]).reshape(-1, self.action_size)
            next_state1 = np.array([next_state]).reshape(-1, self.state_size)
            
            old_val = self.critic_local.model.predict(  [last_state1,action1]  )
            Q_target_next = self.critic_target.model.predict( [next_state1, action1]  )
            
            
            Q_target = reward + self.gamma * Q_target_next * (1 - done)
            
            error = abs(old_val-Q_target)
            
            # Save experience / reward
            self.memory.add(self.last_state, action, reward, next_state, done)
            #self.buffer.memorize(self.last_state, action, reward, done, next_state, error)

            # Learn, if enough samples are available in memory
            #if self.buffer.size() > self.batch_size:
            if(len(self.memory) > self.batch_size):
                experiences = self.memory.sample()
                self.learn(experiences)
                
        # Roll over last state and action
        self.last_state = next_state

    def act(self, state):
        """Returns actions for given state(s) as per current policy."""
        if self.train == True:
            state = np.reshape(state, [-1, self.state_size])
            action = self.actor_local.model.predict(state)[0]
            noise = self.noise.sample()
            self.noise_arr.append(noise)
            matrix = np.asmatrix(np.array(self.noise_arr)) 
            self.noise_matrix = np.array([matrix[:,0],matrix[:,1],matrix[:,2],matrix[:,3]])
            return list(action + noise)  # add some noise for exploration
        else:
            state = np.reshape(state, [-1, self.state_size])
            action = self.actor_local.model.predict(state)[0]
            return list(action)
        
    def progress_test(self, state):
        action = self.actor_local.model.predict(state)[0]
        return list(action)


    def learn(self,experiences): #experiences 
        """Update policy and value parameters using given batch of experience tuples."""
        # Convert experience tuples to separate arrays for each element (states, actions, rewards, etc.)
        states = np.vstack([e.state for e in experiences if e is not None])
        actions = np.array([e.action for e in experiences if e is not None]).astype(np.float32).reshape(-1, self.action_size)
        rewards = np.array([e.reward for e in experiences if e is not None]).astype(np.float32).reshape(-1, 1)
        dones = np.array([e.done for e in experiences if e is not None]).astype(np.uint8).reshape(-1, 1)
        next_states = np.vstack([e.next_state for e in experiences if e is not None])
        
        #states, actions, rewards, dones, next_states, idxs = self.buffer.sample_batch(self.batch_size)
        
        #actions = actions.astype(np.float32).reshape(-1, self.action_size)
        #states = 
        #rewards = rewards.astype(np.float32).reshape(-1, 1)
        #dones = dones.astype(np.uint8).reshape(-1, 1)
        
        # keep training actor local and critic local
        # use values from target model to update and train local
        # don't train target models, we soft update target

        # Get predicted next-state actions and Q values from target models
        #     Q_targets_next = critic_target(next_state, actor_target(next_state))
        
        actions_next = self.actor_target.model.predict_on_batch(next_states) #target
                    
        #Actions predicted by target critic
        Q_targets_next = self.critic_target.model.predict_on_batch([next_states, actions_next]) #target

        # Compute Q targets for current states and train critic model (local)
        Q_targets = rewards + self.gamma * Q_targets_next * (1 - dones)
        
        critic_loss = self.critic_local.model.train_on_batch(x=[states, actions], y=Q_targets)
        
        # Prority
        Q_local = self.critic_local.model.predict_on_batch([next_states, actions_next])
        errors = np.absolute(Q_local - Q_targets)
        
        # update priority
        #for i in range(self.batch_size):
        #    idx = idxs[i]
        #    self.buffer.update(idx, errors[i])
        
        
        #self.tensorboard.on_train_end(None)

        # Train actor model (local)
        action_gradients = np.reshape(self.critic_local.get_action_gradients([states, actions, 0]), (-1, self.action_size))
        actor_loss = self.actor_local.train_fn([states, action_gradients, 1])  # custom training function
        
        self.tensorboard.on_epoch_end(self.batch_id, named_logs(self.critic_local.model, [critic_loss]))
        
        self.batch_id += 1
        

        # Soft-update target models
        self.soft_update(self.critic_local.model, self.critic_target.model)
        self.soft_update(self.actor_local.model, self.actor_target.model)   

    def soft_update(self, local_model, target_model):
        """Soft update model parameters."""
        local_weights = np.array(local_model.get_weights())
        target_weights = np.array(target_model.get_weights())

        assert len(local_weights) == len(target_weights), "Local and target model parameters must have the same size"

        new_weights = self.tau * local_weights + (1 - self.tau) * target_weights
        target_model.set_weights(new_weights)