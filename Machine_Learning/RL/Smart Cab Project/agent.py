import numpy as np
import random
from collections import defaultdict


class Agent:
    def __init__(self, nA=6):
        """ Initialize agent.
        Params
        ======
        - nA: number of actions available to the agent
        """
        self.nA = nA
        self.episodeCount = 1
        self.Q = defaultdict(lambda: np.zeros(self.nA))

    # Sarsa Max (Q Learning)
    def select_action(self, state):
        """ Given the state, select an action.
        Params
        ======
        - state: the current state of the environment

        Returns
        =======
        - action: an integer, compatible with the task's action space
        """
        #epsilon = min(1.0 / self.episodeCount, .0001)
        epsilon = 1.0 / self.episodeCount
        #epsilon = .005

        # GLIE action index
        best_action = np.argmax(self.Q[state])
        # other_action = random.randint(0,nA)

        probs = np.empty(self.nA)
        probs.fill(epsilon / self.nA)
        probs[best_action] = (1 - epsilon) + (epsilon / self.nA)

        action = np.random.choice(np.arange(self.nA), p=probs)

        # print(probs, action)
        # np.random.choice(self.nA)

        return action

    def step(self, state, action, reward, next_state, done):
        """ Update the agent's knowledge, using the most recently sampled tuple.
        Params
        ======
        - state: the previous state of the environment
        - action: the agent's previous choice of action
        - reward: last reward received
        - next_state: the current state of the environment
        - done: whether the episode is complete (True or False)
        """

        alpha = 0.02
        gamma = 1.0

        if (done):
            self.episodeCount += 1

        print("state", state)

        # Q Learning

        self.Q[state][action] += (
            alpha *
            (reward +
             (gamma * self.Q[next_state][np.argmax(self.Q[next_state])]) -
             self.Q[state][action]))

        next_action = self.select_action(state)

        # SARSA
        """
        self.Q[state][action] += (alpha *
                                  (reward +
                                   (gamma * self.Q[next_state][next_action]) -
                                   self.Q[state][action]))
        """
        # self.Q[state][action] += 1
